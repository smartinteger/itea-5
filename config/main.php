<?php

return [
    'components' => [
        'router' => [
            'class' => \core\Router::class,
        ],
        'db' => [
            'class' => \core\Db::class,
        ],
    ],
];;