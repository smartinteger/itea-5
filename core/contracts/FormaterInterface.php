<?php

namespace uks\contracts;
/**
 * Описать интерфейс FormaterInterface 
 * с минимум одним методом format. Написать класс, реализующий данный метод.
 */


interface FormaterInterface
{
    public function getDate();
    public function contextStringify(array $context = []);
    public function format($level, $message, $context = []);

}