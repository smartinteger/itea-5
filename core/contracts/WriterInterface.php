<?php

namespace uks\contracts;
/**
 * Описать интерфейс WriterInterface. Этот интерфейс должен содержать 
 * как минимум один метод write. 
 * Написать класc, реализующий интерфейс WriterInterface. 
 */

interface WriterInterface
{

    /**
     *  FileWriter - пишет в файл
     * @return mixed
     */
    
    public function fileWriter($level, $message, $context);
    public function DbWrider($level, $message, $context);

}