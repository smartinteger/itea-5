<?php

namespace uks\contracts;

interface BootstrapInterface
{
    /**
     * Метод bootstrap будет вызываться после инициализации каждого компонента
     * @return mixed
     */
    public function bootstrap();
  
}