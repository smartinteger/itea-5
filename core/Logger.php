<?php

namespace uks;

use Psr\Log\LoggerInterface;
use uks\contracts\WriterInterface;
use uks\contracts\FormaterInterface;
use uks\Db;
use DateTime;

/**
 * Class Logger, расширяет 
 * абстрактный класс AbstractLogger и реализует интерфейс LoggerInterface.
 */

class Logger extends Writer implements LoggerInterface
{

      /**
     * The underlying logger implementation.
     *
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;   
   

    public function __construct(Writer $logger = null)
    {
        $this->logger = $logger;        
    }
    

    public function write($level, $message, array $context = [])
    {
        //$message = $this->format($level, $message, $context = []) ;
        //file_put_contents("../log/{$level}.log", $message, FILE_APPEND);
        $filePath = "../log/{$level}.log";
        $newformat = new Format();
         
        file_put_contents($filePath, $newformat->format($level, $message, $context). PHP_EOL, FILE_APPEND);

    }

    public function emergency($message, array $context = array())
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Action must be taken immediately.
     *
     * Example: Entire website down, database unavailable, etc. This should
     * trigger the SMS alerts and wake you up.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function alert($message, array $context = array())
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Critical conditions.
     *
     * Example: Application component unavailable, unexpected exception.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function critical($message, array $context = array())
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Runtime errors that do not require immediate action but should typically
     * be logged and monitored.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function error($message, array $context = array())
    {
        $this->writeLog(__FUNCTION__, $message, $context); 
    }

    /**
     * Exceptional occurrences that are not errors.
     *
     * Example: Use of deprecated APIs, poor use of an API, undesirable things
     * that are not necessarily wrong.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function warning($message, array $context = array())
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Normal but significant events.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
   public function notice($message, array $context = [])
    {
       // $this->writeLog(__FUNCTION__, $message, $context);
       echo 200;
    }

    /**
     * Interesting events.
     *
     * Example: User logs in, SQL logs.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function info($message, array $context = array())
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Detailed debug information.
     *
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     */
    public function debug($message, array $context = array())
    {
        $this->writeLog(__FUNCTION__, $message, $context);
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed   $level
     * @param string  $message
     * @param mixed[] $context
     *
     * @return void
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
   
    public function log($level, $message, array $context = array()) {
        $this->writeLog($level, $message, $context);
    }

    protected function writeLog($level, $message, $context)
    {
        $this->logger->{$level}($message = $this->formatMessage($message), $context);

        $this->fireLogEvent($level, $message, $context);
    }
    
}
