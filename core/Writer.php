<?php

namespace uks;

use uks\contracts\WriterInterface;

/**
 * Class Logger, расширяет 
 * абстрактный класс AbstractLogger и реализует интерфейс LoggerInterface.
 */

class Writer implements WriterInterface 
{

    
    public function fileWriter($level, $message, $context)
    {
        
        file_put_contents("../log/{$level}.log", $message, FILE_APPEND);

    }

    public function DbWrider($level, $message, $context)
    {
             //TODO
    }

}
